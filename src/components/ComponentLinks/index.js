import React from 'react';
import { Link, BrowserRouter as Router } from 'react-router-dom';

const ComponentLinks = (props) => (
  <>
    Авторизоция
    <ul>
      <li>
        <Link to="/register">Регистрация</Link>
      </li>
      <li>
        <Link to="/login">Авторизация</Link>
      </li>
      <li>
        <Link to="/updatePassword">Обновить пароль</Link>
      </li>
      <li>
        <Link to="/forgotten">Забыли пароль?</Link>
      </li>
    </ul>
    Общение
    <ul>
      <li>
        <Link to="/active">Активные</Link>
      </li>
      <li>
        <Link to="/start">Приступить</Link>
      </li>
      <li>
        <Link to="/finished">Завершенные</Link>
      </li>
    </ul>
  </>
);

export default ComponentLinks;

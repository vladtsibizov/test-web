import React from 'react';
import dialogs from '../../../assets/dialogs';

const queueStyle = {
  fontSize: '200%'
};
const dialogItemStyle = {
  marginBottom: '-1px'
};

const DialogItem = ({ item }) => (
  <div className="row border border-dark p-3" style={dialogItemStyle}>
    <div className="col-2">
      <img
        src="https://obychenie.herokuapp.com/images/%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0/u3.png"
      />
      <p>{item.name}</p>
    </div>
    <div className="col-7">
      {item.text}
    </div>
    <div className="col-3">
      <div>Продолжить</div>
      <div>{item.assigned}</div>
      <div>
        Сохранить
        <img
          src="https://obychenie.herokuapp.com/images/%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0/u3.png"
          className="w-25"
        />
      </div>
    </div>
  </div>
);

const DialogItems = (props) => {
  return (
    <div className="p-5">
      <div className="font-weight-bold" style={queueStyle}>
        {typeof props.queueNumber === 'undefined' && `Клиентов в очереди: ${props.queueNumber}`}
      </div>
      {dialogs.map((item, i) => <DialogItem key={i} item={item} />)}
    </div>
  );
};

export default DialogItems;

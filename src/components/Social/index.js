import React, { Component } from 'react';
import css from './css/style.module.css';
import Active from "./Active";
import Start from "./Start/Start";
import Finished from "./Finished";

const SideBarItem = ({ text }) => {
  return (
    <div className="border-bottom border-dark pb-2 text-center pt-4">
      <img alt="" src="https://obychenie.herokuapp.com/images/%D0%BD%D0%B0%D1%87%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D1%80%D0%B0%D0%BD%D0%B8%D1%86%D0%B0/u3.png" />
      <p>{text}</p>
    </div>
  );
};

const SideBar = (props) => {
  const style = {
    marginRight: '-1px'
  };
  return (
    <div className="border border-dark col-2 p-0" style={style}>
      <SideBarItem text="Активные"/>
      <SideBarItem text="Завершенные"/>
      <SideBarItem text="Сохраненные"/>
      <div className={css.EmptyDiv}></div>
    </div>
  );
};

class Social extends Component {
  render() {
    return (
      <div className={`row ${css.Row} ${css.SocialContainer} small m-0`}>
        <SideBar />
        <div className="border border-dark col-10 pb-5">
          <div className="py-3 text-right">
            <div>
              <b>operator@mail.ru</b>
              <button className="w-25">Выход</button>
            </div>
            <div className="mt-2">
              <span className="text-muted">Поиск:</span><input />
            </div>
          </div>
          {this.props.status === "active" && <Active />}
          {this.props.status === "start" && <Start queueNumber={this.props.queueNumber} />}
          {this.props.status === "finished" && <Finished />}
        </div>
      </div>
    );
  }
};

export default Social;

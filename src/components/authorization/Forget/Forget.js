import React, { Component } from "react";
import { renderField } from '../../../config/renderField';
import { Field, reduxForm } from 'redux-form';
import AuthStyles from '../authorizatoin.module.css';

class Forget extends Component {
  render() {
    return (
      <form
        className={`${AuthStyles.Form} d-inline-block border border-dark`}
        autoComplete="off"
        id="LoginForm"
        name="LoginForm"
      >
        <h3>Забыли пароль</h3>
        <Field
          component={renderField}
          name="email"
          id="email"
          type="text"
          label="Email"
          divClassName="mb-5"
        />
        <button
          type="submit"
          className="mt-5"
        >
          Отправить ссылку для восстановления
        </button>
        <div className="row mt-5">
          <a href="/login" className="col small">Войти</a>
          <a href="/register" className="col small">Регистрация</a>
        </div>
      </form>
    );
  }
};

Forget = reduxForm({
  form: 'forget'
})(Forget);

export default Forget;

import React, { Component } from "react";
import { SubmissionError } from "redux-form";
import Forget from "./Forget";
import connect from "react-redux/es/connect/connect";

class ForgetMain extends Component {
  render() {
    return (
      <Forget />
    );
  }
};

export default connect(
  null,
  null
)(ForgetMain);

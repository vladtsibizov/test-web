import React, { Component } from "react";
import { SubmissionError } from "redux-form";
import connect from "react-redux/es/connect/connect";
import RegistrationForm from './RegistrationForm';

const submit = async (values) => {
  const { email, password } = values;
  const body = JSON.stringify({
    CreateUserDto: { email, password }
  });
  console.log(body)
  const res = await fetch('http://142.93.103.89/register', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify({ email, password })
  }).then(res => res.json());
  if (res.statusCode === 401) {
    throw new SubmissionError({ _error: 'Authentication failed!' });
  }
  if (res.id) {
    window.location.href = '/';
  }
};

const Registration = () => {
  return <RegistrationForm onSubmit={submit} />;
};

export default connect(
  null,
  null
) (Registration);

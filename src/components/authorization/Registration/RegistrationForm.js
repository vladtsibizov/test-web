import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../../config/renderField';
import AuthStyles from '../authorizatoin.module.css';

const validate = ({ email, password, password2 }) => {
  const errors = {};
  if (!email) {
    errors.email = 'Введите email';
  }
  const emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!emailExp.test(email)) {
    errors.email = 'Некорректный email';
  }
  if (!password) {
    errors.password = 'Введите пароль';
  } else if (
    password.search(/[A-Z]/) === -1 ||
    password.search(/[a-z]/) === -1 ||
    password.search(/[0-9]/) === -1 ||
    password.length < 8
  ) {
    errors.password = 'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков';
  } else if (password !== password2) {
    errors.password2 = 'Пароли не совпадают';
  }
  return errors;
};


class RegistrationForm extends Component {
  render() {
    return (
      <form
        className={`${AuthStyles.Form} d-inline-block border border-dark`}
        onSubmit={this.props.handleSubmit}
        autoComplete="off"
        id="RegistrationForm"
        name="RegistrationForm"
      >
        <h3>Регистрация</h3>
        <Field
          component={renderField}
          name="email"
          type="text"
          label="Email"
          сlassName="mt-5"
        />
        <Field
          component={renderField}
          name="password"
          type="password"
          label="Пароль"
          className="mt-3"
        /><Field
          component={renderField}
          name="password2"
          type="password"
          label="Подтверждение пароля"
          className="mt-3"
        />
        <button
          type="submit"
          className="mt-5"
        >
          Регистрация
        </button>
        <div className="mt-3">
          <a href="/login" className="col text-left small pl-0">Войти</a>
        </div>
      </form>
    );
  }
};

RegistrationForm = reduxForm({
  form: 'registration',
  validate
}) (RegistrationForm);

export default RegistrationForm;

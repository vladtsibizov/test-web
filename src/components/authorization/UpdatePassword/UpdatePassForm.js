import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { renderField } from '../../../config/renderField';
import AuthStyles from '../authorizatoin.module.css';

class UpdatePassForm extends Component {
  render() {
    return (
      <form
        className={`${AuthStyles.Form} d-inline-block border border-dark`}
        onSubmit={null}
        autoComplete="off"
        id="UpdatePassForm"
        name="UpdatePassForm"
      >
        <h3>Обновить пароль</h3>
        <Field
          component={renderField}
          name="password"
          type="password"
          label="Пароль"
          divClassName="mt-3"
        />
        <Field
          component={renderField}
          name="password2"
          type="password2"
          label="Подтверждение пароля"
          divClassName="mt-3"
        />
        <button
          type="submit"
          className="mt-5"
        >
          Отправить ссылку для восстановления
        </button>
        <div
          className={`row ${AuthStyles['auth-footer']} mt-5`}
        >
          <a className="col">Войти</a>
          <a className="col">Регистрация</a>
        </div>
      </form>
    );
  }
}

UpdatePassForm = reduxForm({
  form: 'registration'
}) (UpdatePassForm);

export default UpdatePassForm;

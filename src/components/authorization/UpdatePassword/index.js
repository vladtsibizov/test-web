import React, { Component } from "react";
import { SubmissionError } from "redux-form";
import connect from "react-redux/es/connect/connect";
import UpdatePassForm from './UpdatePassForm';

const UpdatePassword = () => {
  return <UpdatePassForm />;
};

export default connect(
  null,
  null
) (UpdatePassword);

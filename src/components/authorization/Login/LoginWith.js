import React from "react";

const LoginWith = ({ link, img, text }) => (
  <div className="mt-3">
    <a href={link}>
      <img style={{ width: "20%" }} src={img} alt="" />
      <div>{text}</div>
    </a>
  </div>
);


export default LoginWith;

import React, { Component } from "react";
import { SubmissionError } from "redux-form";
import LoginForm from "./LoginForm";
import connect from "react-redux/es/connect/connect";

class Login extends Component {
  submit = async (values) => {
    const res = await fetch('http://142.93.103.89/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values)
    }).then(res => res.json());
    if (res.statusCode === 401) {
      throw new SubmissionError({_error: 'Authentication failed!'});
    } else if (res.access_token) {
      window.location.href = '/';
    }
  };
  render() {
    return (
      <LoginForm onSubmit={this.submit} />
    );
  }
};

export default connect(
  null,
  null
)(Login);

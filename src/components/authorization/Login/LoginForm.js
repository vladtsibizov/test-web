import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { validate } from '../../../config/validate';
import { renderField } from '../../../config/renderField';
import AuthStyles from '../authorizatoin.module.css';
import LoginWith from "./LoginWith";

class LoginForm extends Component {
  render() {
    const { error, handleSubmit } = this.props;
    return (
      <form
        className={`${AuthStyles.Form} d-inline-block border border-dark`}
        onSubmit={handleSubmit}
        autoComplete="off"
        id="LoginForm"
        name="LoginForm"
      >
        <h3>Авторизация</h3>
        <Field
          component={renderField}
          name="email"
          type="text"
          label="Email"
          divClassName="mt-5"
        />
        <Field
          component={renderField}
          name="password"
          type="password"
          label="Пароль"
          divClassName="mt-3"
        />
        <button
          type="submit"
          className="mt-5"
        >
          Войти
        </button>
        <div className={`mt-1 row ${AuthStyles['auth-footer']}`}>
          <div className="col">
            <LoginWith
              img="https://vk.com/favicon.ico"
              link="https://vk.com"
              text="Войти через VK"
            />
            <a href="/register">Зарегистрироваться</a>
          </div>
          <div className="col">
            <LoginWith
              img="https://google.com/favicon.ico"
              link="https://google.com"
              text="Войти через Google"
            />
            <a href="/forgotten">Забыли пароль?</a>
          </div>
        </div>
        <div className="err">
          {(error && <span className="text-danger">{error}</span>)}
        </div>
      </form>
    );
  }
}

LoginForm = reduxForm({
  form: 'login',
  validate
}) (LoginForm);

export default LoginForm;

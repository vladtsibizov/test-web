import React from 'react';
import { Route, Switch } from "react-router-dom";
import UpdatePassword from './components/authorization/UpdatePassword';
import Registration from './components/authorization/Registration';
import Login from './components/authorization/Login';
import Forget from './components/authorization/Forget';
import Social from './components/Social';
import Dashboard from './components/Dashboard';

class App extends React.Component {
  render() {
    return (
      <div className="container my-5">
        <Switch>
          <Route exact path="/">
            <Dashboard />
          </Route>
          <Route path="/register">
            <Registration />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/updatePassword">
            <UpdatePassword />
          </Route>
          <Route path="/forgotten">
            <Forget />
          </Route>
          <Route path="/active">
            <Social status="active"/>
          </Route>
          <Route path="/start">
            <Social status="start"/>
          </Route>
          <Route path="/finished">
            <Social status="finished"/>
          </Route>
        </Switch>
      </div>
    );
  }
}

export default App;

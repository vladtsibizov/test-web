export const AUTH_OK = 'AUTH_OK';
export const AUTH_FAIL = 'AUTH_FAIL';
export const TEST = 'TEST';

export const SEND_LOGIN_DATA = 'SEND_LOGIN_DATA';
export const sendLoginData = (data) => ({ type: SEND_LOGIN_DATA, data });

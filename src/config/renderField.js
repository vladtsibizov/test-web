import React from 'react';

export const renderField = ({ input, divClassName, label, type, id, meta: { touched, error } }) => (
  <div className={divClassName}>
    <label htmlFor={input.name} className="text-secondary small">{label}</label>
    <input
      {...input}
      id={input.name}
      type={type}
    />
    {touched &&
      (error && <span className="text-danger">{error}</span>)}
  </div>
);

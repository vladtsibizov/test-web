export const validate = ({ email, password }) => {
  const errors = {};
  if (!email) {
    errors.email = 'Введите email';
  }
  const emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!emailExp.test(email)) {
    errors.email = 'Некорректный email';
  }
  if (!password) {
    errors.password = 'Введите пароль';
  } else if (
    password.search(/[A-Z]/) === -1 ||
    password.search(/[a-z]/) === -1 ||
    password.search(/[0-9]/) === -1 ||
    password.length < 8
  ) {
    errors.password = 'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков';
  }
  return errors;
};


import { createStore, combineReducers, applyMiddleware } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import auth from '../reducers/auth';

const reducers = { form: formReducer, auth };

const rootReducer = combineReducers(reducers);

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

store.subscribe(() => {
  // console.log(store.getState());
});

export default store;

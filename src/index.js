import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import loginStore from './store/login.js';

ReactDOM.render(
    <Provider store={loginStore}>
      <Router>
        <App />
      </Router>
    </Provider>
  , document.getElementById('root'));

import { AUTH_OK, AUTH_FAIL, TEST } from '../actions/auth';

function auth(state = {}, action) {
  switch (action.type) {
    case AUTH_OK:
      return {
        ...state,
        ok: true
      };
    case AUTH_FAIL:
      return {
        ...state,
        ok: false
      };
    case TEST:
      return {
        ...state,
        TEST: true
      };
    default:
      return state;
  }
}

export default auth;
